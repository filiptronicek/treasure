import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import {
	loginGoogle,
	authRef,
	usersRef
} from "../../config/firebase";

class Login extends Component {
	state = {
		user: null
	}

	componentDidMount() {
		this.getAuthStatus();
	}

	handleLoginWithGoogle = () => {
		loginGoogle()
			.then(({ user }) => {
				this.setState({ user });
				this.validateUser(user);
			})
			.catch(err => console.log('login with google error: ', err));
	}

	getAuthStatus = () => {
		authRef.onAuthStateChanged(user => this.setState({ user }));
	}

	createUser = (user) => {
		usersRef.push({
			name: user.displayName,
			email: user.email,
			photo: user.photoURL ? user.photoURL : 'http://placehold.it/100x100'
		});
	}

	validateUser = (user) => {
		usersRef.once('value').then(snap => {
			if (!snap.val()) {
				this.createUser(user);
			} else {
				const usersArr = Object.values(snap.val());
				const userExist = usersArr.filter(item => item.email === user.email)[0];
				if (!userExist) {
					this.createUser(user);
				}
			}
		});
	}

	logout = () => {
		authRef.signOut()
			.then(() => console.log('success logout'))
			.catch(err => console.log('error logout: ', err));
	}

	render() {
		const { user } = this.state;
		return(
			<div>
				{
					user ? (
						<React.Fragment>
							{user.photoURL ? <img src={user.photoURL} alt="profile" width={100} height={100} /> : null}
							<Button primary onClick={this.props.createTreasure}>Mis proyectos</Button>
							<Button secondary onClick={this.logout}>logout</Button>
						</React.Fragment>):
							<Button primary onClick={this.handleLoginWithGoogle}>Login</Button>
				}
			</div>
		);
	}
}

export default Login;
