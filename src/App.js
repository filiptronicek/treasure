import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Homepage from './containers/Homepage/';
import Treasure from './containers/Treasure';
import TreasureDetail from './containers/TreasureDetail';

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Homepage}/>
          <Route exact path="/treasure/" component={Treasure}/>}
          <Route exact path="/treasure/:id" component={TreasureDetail}/>}
          <Route render={ () => <h1>404 Error</h1> } />
        </Switch>
      </div>
    );
  }
}

export default App;
