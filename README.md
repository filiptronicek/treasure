TREASURE APP

## Instructions

For run this project follow this steps:

### `npm install`

- This project use firebase for store data.
- You need create a .env file in the root path with this structure.

```
REACT_APP_FIREBASE_APIKEY = 'REPLACE_DATA_HERE'
REACT_APP_FIREBASE_AUTHDOMAIN = 'REPLACE_DATA_HERE'
REACT_APP_FIREBASE_DATABASEURL = 'REPLACE_DATA_HERE'
REACT_APP_FIREBASE_PROJECTID = 'REPLACE_DATA_HERE'
REACT_APP_FIREBASE_STORAGEBUCKET = 'REPLACE_DATA_HERE'
```

For get this data, you need to create a firebase app, and also you need
activate `google auth` for your firebase app.

Then, you can run: 

### `npm start`

if you've any doubts, write me to : edades@gmail.com